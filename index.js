const amqp = require('amqplib/callback_api');
const https = require('https');
const fs = require('fs');
const FormData = require('form-data');
const fetch = require("node-fetch");

const dir = 'downloads'

const download = function (url, dest) {
    return new Promise((resolve, reject) => {
        const file = fs.createWriteStream(dest, { flags: "wx" });

        const request = https.get(url, response => {
            if (response.statusCode === 200) {
                response.pipe(file);
            } else {
                file.close();
                fs.unlink(dest, () => {}); // Delete temp file
                reject(`Server responded with ${response.statusCode}: ${response.statusMessage}`);
            }
        });

        request.on("error", err => {
            file.close();
            fs.unlink(dest, () => {}); // Delete temp file
            reject(err.message);
        });

        file.on("finish", () => {
            resolve();
        });

        file.on("error", err => {
            file.close();
            fs.unlink(dest, () => {}); // Delete temp file
            reject(err.message);
        });
    });
}

const deleteAllFiles = function(directory) {
    const files = fs.readdirSync(directory);
    for (const file of files) {
        fs.unlink(`${directory}/${file}`, err => {
            if (err) throw err;
        });
    }
}

amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = '1606879230_DIRECT';
        var routingKey = "SagabRoutingKey";
        channel.assertExchange(exchange, 'direct', {
            durable: false
        });
        channel.assertQueue('', {
            exclusive: true
        }, function(error2, q) {
            if (error2) {
                throw error2;
            }
            console.log(' [*] Waiting for logs. To exit press CTRL+C');
            channel.bindQueue(q.queue, exchange, routingKey);
            channel.consume(q.queue, function(msg) {
                console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
                // Download file here
                const receivedJson = JSON.parse(msg.content.toString())
                const fileLinks = [
                    receivedJson.file1,
                    receivedJson.file2,
                    receivedJson.file3,
                    receivedJson.file4,
                    receivedJson.file5,
                    receivedJson.file6,
                    receivedJson.file7,
                    receivedJson.file8,
                    receivedJson.file9,
                    receivedJson.file10,
                ];
                let i = 1;
                let downloadPromise = []
                fileLinks.forEach(function(link) {
                    const extension = link.split('.').pop();
                    console.log(link);
                    downloadPromise.push(download(link, String(`${dir}/${i}.${extension}`)));
                    i++;
                });
                Promise.all(downloadPromise).then(() => {
                    console.log("All files downloaded");
                    // Send to server 3
                    const formData = new FormData();

                    let files = fs.readdirSync(`./${dir}/`);
                    console.log(files);
                    for (let file of files) {
                        formData.append('files', fs.createReadStream(`${dir}/${file}`));
                    }
                    const options = {
                        method: "POST",
                        body: formData
                    };
                    console.log("Fetching")
                    fetch('http://localhost:3001/', options).then(res => {
                        console.log("File sent to server 3");
                        deleteAllFiles(dir);
                        console.log("Done");
                    }).catch(err => {
                        console.log(err);
                    });
                });
                
            }, {
                noAck: true
            });
        });
    });
});
